import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { store } from "./store/";

import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret, faSpinner, faUserAlt, faHome, faCheckCircle, faHeart, faClock, faFunnelDollar, faCheck, faDollarSign } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faUserSecret, faSpinner, faUserAlt, faHome, faCheckCircle, faHeart, faClock, faFunnelDollar, faCheck, faDollarSign)
 
const app = createApp(App);
app.component('font-awesome-icon', FontAwesomeIcon)
app.use(router)
app.use(store)
app.mount("#app");
